# -*- coding: utf-8 -*-
"""
remote_storage

Helper functions to handle remote storage.

* created: 2013-08-14 Kevin Chan <kefin@makedostudio.com>
* updated: 2013-08-14 kchan
"""

import os.path

from django.core.files.storage import default_storage, FileSystemStorage
from django.core.files import File

# filebrowser imports
from filebrowser.settings import *


# local storage
# * this is used to save a copy of the upload file to the local file system
#   while the a mirrot version is saved to remote storage.
file_system_storage = FileSystemStorage()


def local_path_to_remote_url(local_path):
    """
    Convert a local path to a remote media url.
    """
    media_url = REMOTE_MEDIA_URL
    local_path_prefix = MEDIA_ROOT
    remote_path_prefix = REMOTE_UPLOAD_PATH_PREFIX
    relpath = local_path.replace(local_path_prefix, '')
    url = '%s%s%s' % (media_url, remote_path_prefix, relpath)
    return url


def get_remote_path(local_path):
    """
    Helper function to convert local path to remote path.
    * if USE_REMOTE_STORAGE is True.
    """
    local_path_prefix = MEDIA_ROOT
    remote_path_prefix = REMOTE_UPLOAD_PATH_PREFIX
    relpath = local_path.replace(local_path_prefix, '')
    remote_path = '%s%s' % (remote_path_prefix, relpath)
    return remote_path


def save_local_file(path, file):
    """
    Save file to local file system.
    """
    local_file = file_system_storage.save(path, file)
    return local_file


def get_img_ext(path, default_ext='unknown'):
    """
    Detect image type from file path and return file extension.
    """
    import imghdr
    imgtype = imghdr.what(path)
    suffix = {
        'rgb': 'rgb',
        'gif': 'gif',
        'pbm': 'pbm',
        'pgm': 'pgm',
        'ppm': 'ppm',
        'tiff': 'tif',
        'rast': 'rast',
        'xbm': 'xbm',
        'jpeg': 'jpg',
        'bmp': 'bmp',
        'png': 'png'
    }
    return suffix.get(imgtype, default_ext)


def copy_to_remote(path, storage=None):
    """
    Push a local file to remote media storage.

    :param path: local path of file
    :param storage: storage instance to use (default: default_storage)
    :returns: url of file on remote storage server
    """
    FREAD_CHUNK_SIZE = 64 * 2**10
    UNKNOWN_MIME_TYPE = 'application/x-octet-stream'
    MIME_TYPES = {
        'gif': 'image/gif',
        'jpg': 'image/jpeg',
        'png': 'image/png',
        'bmp': 'image/bmp',
        'tiff': 'image/tiff',
        'unknown': UNKNOWN_MIME_TYPE,
    }

    if not path or not os.path.isfile(path):
        remote_path = path
    else:
        if not storage:
            storage = default_storage

        media_url = REMOTE_MEDIA_URL
        media_root = MEDIA_ROOT
        subpath = path.replace(media_root, '')
        fkey = '%s%s' % (REMOTE_UPLOAD_PATH_PREFIX, subpath)

        # determine mime type
        fext = get_img_ext(path)
        content_type = MIME_TYPES.get(fext, UNKNOWN_MIME_TYPE)

        # write file to remote server
        file = storage.open(fkey, 'w')
        storage.headers.update({"Content-Type": content_type})
        f = open(path, 'rb')
        media = File(f)
        for chunk in media.chunks(chunk_size=FREAD_CHUNK_SIZE):
            file.write(chunk)
        file.close()
        media.close()
        f.close()

        # construct remote url
        remote_path = '%s%s' % (media_url, subpath)

    return remote_path


def delete_remote(url, storage=None):
    """
    Delete file on remote storage.
    """
    if storage is None:
        storage = default_storage
    media_url = REMOTE_MEDIA_URL
    path = url.replace(media_url, '')
    storage.delete(path)


def rename_remote(old_local_path, new_local_path):
    """
    Rename remote file.
    * delete the old one
    * and copy the new one to remote
    """
    delete_remote(local_path_to_remote_url(old_local_path))
    copy_to_remote(new_local_path)
