django-filebrowser-no-grappelli-and-uploadify
=============================================

This is a fork of the "django-filebrowser-no-grappelli-and-uploadify"
repository from github, modified to work with Django 1.4. This version
of filebrowser is not synced with the latest django-filebrowser and does
not contain any latest features from the main branch. This is a "plain
jane" variant of the filebrowser that does not depend on grapelli or a
flash uploader.

Original repository on github:

* https://github.com/alexvasi/django-filebrowser-no-grappelli-and-uploadify


## this version

* modifed by Kevin Chan <kefin@makedostudio.com>
* tested only with Django versions > 1.4.
